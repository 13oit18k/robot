package ru.robot;

/**
 * Demo- класс для реализации движения робота
 *
 * @author Алексей Хохлов
 */
public class Demo {
    public static void main(String[] args) {
        Robot droid = new Robot();

        System.out.println("Начальные координаты робота - " + droid.getPlaceOne());
        System.out.println("Место назначения робота - " + droid.getPlaceTwo());
        System.out.println("По умолчанию робот смотрит - " + droid.getTurn() + "\n");

        droid.Move();
    }
}