package ru.robot;

/**
 * Класс для описание точки координат
 *
 * @author Максим Евстигнеев
 */
public class Point {
    int x;
    int y;

    /**
     * Конструктор по умолчанию, устанавливает:
     * x = 0.0
     * y = 0.0
     */
    public Point() {
        this.x = 0;
        this.y = 0;
    }

    /**
     * Конструтор с параметрами
     * @param x абцисса
     * @param y ордината
     */
    public Point(int x, int y) {
        this.x = x;
        this.y = y;
    }

    /**
     * Геттер абциссы
     *
     * @return абцисса
     */
    public int getX() {
        return x;
    }

    /**
     * Геттер ординаты
     *
     * @return ордината
     */
    public int getY() {
        return y;
    }

    /**
     * Вывод значений точки
     *
     * @return String
     */

    @Override
    public String toString() {
        return "{" + x + "," + y + '}';
    }
}