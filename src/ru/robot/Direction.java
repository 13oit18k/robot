package ru.robot;

/**
 * Класс-перечисления для напрапления робота
 *
 * @author Алексей Хохлов
 */
public enum Direction {Right, Left, Down, Up}
