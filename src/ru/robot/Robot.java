package ru.robot;

/**
 * Класс для реализации движения робота
 *
 * @author Вадим Дунькин
 */
public class Robot {
    private Point placeOne;
    private Point placeTwo;
    private Direction turn;


    /**
     * Конструктор для инициализации
     * начальных и конечных координат робота
     */
    public Robot() {
        this.placeOne = new Point(4, 5);
        this.placeTwo = new Point(-8, 8);
        this.turn = Direction.Up;
    }

    /**
     * Геттер первоначального местоположения робота
     *
     * @return координаты первоначального местоположения
     */
    public Point getPlaceOne() {
        return placeOne;
    }

    /**
     * Геттер конечного местоположения робота
     *
     * @return координаты конечного местоположения
     */
    public Point getPlaceTwo() {
        return placeTwo;
    }

    /**
     * Геттер взгляда робота
     *
     * @return взгяд робота
     */
    public Direction getTurn() {
        return turn;
    }

    /**
     * Метод реализующщий движение робота вперед, в зависимости от того, куда он смотрит
     */
    public void Move() {
        int x = placeOne.getX();
        int y = placeOne.getY();

        if (placeTwo.getX() == x && placeTwo.getY() == y) {
            System.out.println("Робот стоит на месте");
        }

        if (x != placeTwo.getX()) {
            if (x < placeTwo.getX()) {
                turnRight(this.turn = Direction.Right);
                while (x != placeTwo.getX()) {
                    x++;
                    System.out.println("Шаг вперед" + '{' + x + "," + y + '}');
                }
            } else {
                turnLeft(this.turn = Direction.Left);
                while (x != placeTwo.getX()) {
                    x--;
                    System.out.println("Шаг вперед" + '{' + x + "," + y + '}');
                }
            }
        }

        if (y != placeTwo.getY()) {
            if (y < placeTwo.getY()) {
                if (this.turn != Direction.Up) {
                    if (this.turn == Direction.Right) {
                        turnLeft(this.turn = Direction.Up);
                    } else {
                        turnRight(this.turn = Direction.Up);
                    }
                }
                while (y != placeTwo.getY()) {
                    y++;
                    System.out.println("Шаг вперед" + '{' + x + "," + y + '}');
                }
            } else {
                if (this.turn == Direction.Up) {
                    turnLeft(this.turn = Direction.Left);
                    turnLeft(this.turn = Direction.Down);
                } else if (this.turn == Direction.Right) {
                    turnRight(this.turn = Direction.Down);
                } else {
                    turnLeft(this.turn = Direction.Down);
                }
                while (y != placeTwo.getY()) {
                    y--;
                    System.out.println("Шаг вперед" + '{' + x + "," + y + '}');
                }
            }
        }

        System.out.println("\nКонечная точка достингнута - " + '{' + x + "," + y + '}');
    }

    /**
     * Метод поворота робота налево
     *
     * @param turn направление поворота Direction.Left или Direction.Down
     */
    public void turnLeft(Direction turn) {
        switch (turn) {
            case Left:
                System.out.println("Робот повернул налево, и стал смотреть - " + turn);
                break;
            case Down:
                System.out.println("Робот повернул налево, и стал смотреть - " + turn);
                break;
            case Up:
                System.out.println("Робот повернул налево, и стал смотреть - " + turn);
                break;
        }
    }

    /**
     * Метод поворота робота направо
     *
     * @param turn направление поворота Direction.Right или Direction.Up
     */
    public void turnRight(Direction turn) {
        switch (turn) {
            case Right:
                System.out.println("Робот повернул направо, и стал смотреть - " + turn);
                break;
            case Up:
                System.out.println("Робот повернул направо, и стал смотреть - " + turn);
                break;
            case Down:
                System.out.println("Робот повернул направо, и стал смотреть - " + turn);
                break;
        }
    }
}


