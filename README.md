# Введение #

## Назначение ##

Данный проект предназначен для демонстрации проекта «Робот».

## Область применения: ##

Даная программа может применяться в процессе изучения различных методов в Java.

## Ссылки: ##

Javadoc: https://ru.wikipedia.org/wiki/Javadoc

## Общее описание: ##

Особенности продукта:

* Консольное приложение;
* Операционная среда: Windows 7 и выше.
* Среда разработки: IntelliJ IDEA;
* Интерфейс программирования: JRE и JDK;
* Язык программирования: Java;
* Аппаратные средства, обеспечивающие поддержку выше перечисленного ПО.

## Функциональные требования: ##

Программа должна выполнять следующие действия: 

1. Иницилизировать робота.
2. Передвигать робота из начальной координаты в конечную( по осям x и y).
3. Поворачивать робота на 90 градусов вправо и влево.
4. Поворачивать робота на 90 градусов влево.
5. Передвигать робота на 1 шаг вперед.

## Разработать программу, содержащую в себе несколько классов: ##

**Класс – Point.**

Имеет конструкторы: 

* Конструктор по умолчанию, для задачи начальных координат робота по умолчанию.
* Конструктор с параметрами, тип целочисленный, для задачи координат робота.

Имеет поля:

* x(координата x), тип целочисленный, единица измерения: число, предназначается для координаты х.
* y(координата y), тип целочисленный, единица измерения: число, предназначается для координаты y.
* Входными данными является число.

**Класс Direction.**

Класс для перечисления направления робота:

* Right-право
* Left-лево
* Down-вниз
* Up-вверх.

**Класс Robot.**


Имеет конструкторы: 

* Конструктор по умолчанию, для задачи начальных и конечных координат робота.

Имеет методы: 

* "движение вперёд" - передвигает робота на 1 шаг вперёд.
* "поворот налево" - поворачивает робота на 90 градусов, влево.
* "поворот направо" - поворачивает робота на 90 градусов, вправо.

Имеет поля:

* «начальные координаты робота», тип целочисленный, единица измерения: число, предназначен для ввода начальных координат.
* «конечные координаты робота», тип целочисленный, единица измерения: число, предназначен для ввода конечных координат.

**Демонстрационный класс Demo.**

* Класс Demo, выводит информацию о передвижении робота.

## Требования к входным данным: ##

* Входными данными будут:
	- начальные координаты робота(целочисленного типа в декартовой системе координат(x;y)),
	- конечные координаты робота(целочисленного типав декартовой системе координат(x;y)),
	- направление робота(робот может смотреть вверх/вниз/вправо/влево).
* Входные данные инициализируются в классе Robot.

## Требования к выходным данным: ##

Промежуточными результатами работы программы будут являться сообщения: 

* <Поворот налево/направо
* <Шаг вперед{x,y}>
* Результатом выполнения программы является сообщение, о том, что робот достиг цели.
* <Конечная точка достингнута - {x,y}> - результат выполнения программы.

*Ответ выводится на консоль.*

## Требования к интерфейсу пользователя: ##

**Пользовательский интерфейс:**
		
Вид интерфейса – консольный, язык интерфейса – русский.